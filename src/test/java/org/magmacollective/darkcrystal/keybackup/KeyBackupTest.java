package org.magmacollective.darkcrystal.keybackup;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests for the key backup class
 *
 * @author Magma Collective
 */
public class KeyBackupTest {

  private HashMap<byte[], KeyBackup> createCustodians(int numberCustodians) throws Exception {
    // Make a map of public keys to custodians
    HashMap<byte[], KeyBackup> custodians = new HashMap<>();
    for (int i = 0; i < numberCustodians; i++) {
      KeyBackup custodian = new KeyBackup();
      custodians.put(custodian.getPublicEncryptionKey(), custodian);
    }
    return custodians;
  }

  /**
   * Basic test
   */
  @Test
  public void test() throws Exception {
    byte[] secret = "its nice to be important but its more important to be nice".getBytes();
    final KeyBackup secretOwner = new KeyBackup();
    assertEquals("Public key of correct length available", secretOwner.getPublicEncryptionKey().length, 32);
    final int numberCustodians = 4;
    final int threshold = 3;

    HashMap<byte[], KeyBackup> custodians = createCustodians(numberCustodians);

    // The secret owner creates a secret with a threshold of 3
    List<byte[]> custodianPublicKeys = new ArrayList<>(custodians.keySet());
    OwnSecret newSecret = secretOwner.createSecret(secret, "a great secret", threshold, custodianPublicKeys);
    assertEquals("Secret owner has one secret", secretOwner.getOwnSecrets().size(), 1);

    // We 'send' the shard messages to each custodian
    for (byte[] custodianPublicKey : custodianPublicKeys) {
      KeyBackup custodian = custodians.get(custodianPublicKey);
      custodian.receiveDarkCrystalMessage(
              newSecret.getShardMessage(custodianPublicKey),
              secretOwner.getPublicEncryptionKey(),
              secretOwner.getPublicSigningKey()
      );
    }

    // At this point the secret owner looses their device, and needs to recover
    // Lets call the new secret owner newSecretOwner
    final KeyBackup newSecretOwner = new KeyBackup();

    // Each custodian 'forwards' their shard to the new secret owner
    for (byte[] custodianPublicKey : custodianPublicKeys) {
      KeyBackup custodian = custodians.get(custodianPublicKey);
      byte[] forwardMessage = custodian.forwardShard(newSecret.getRootId(), newSecretOwner.getPublicEncryptionKey());
      newSecretOwner.receiveDarkCrystalMessage(forwardMessage, custodianPublicKey, custodian.getPublicSigningKey());
    }

    // the new secret owner
    byte[] recoveredSecret = newSecretOwner.getOwnSecret(newSecret.getRootId()).combine();
    assertTrue("Secret successfully recovered", Arrays.equals(secret, recoveredSecret));
  }

  @Test()
  public void testUnmetThreshold() throws Exception {
    byte[] secret = "its nice to be important but its more important to be nice".getBytes();
    final KeyBackup secretOwner = new KeyBackup();
    assertEquals("Public key of correct length available", secretOwner.getPublicEncryptionKey().length, 32);
    final int numberCustodians = 4;
    final int threshold = 3;

    HashMap<byte[], KeyBackup> custodians = createCustodians(numberCustodians);

    // the secret owner creates a secret with a threshold of 3
    List<byte[]> custodianPublicKeys = new ArrayList<>(custodians.keySet());
    OwnSecret newSecret = secretOwner.createSecret(secret, "a great secret", threshold, custodianPublicKeys);
    assertEquals("secret owner has one secret", secretOwner.getOwnSecrets().size(), 1);

    // we 'send' the shard messages to each custodian
    for (byte[] custodianPublicKey : custodianPublicKeys) {
      KeyBackup custodian = custodians.get(custodianPublicKey);
      custodian.receiveDarkCrystalMessage(
              newSecret.getShardMessage(custodianPublicKey),
              secretOwner.getPublicEncryptionKey(),
              secretOwner.getPublicSigningKey()
      );
    }

    // at this point the secret owner looses their device, and needs to recover
    // lets call the new secret owner newSecretOwner
    final KeyBackup newSecretOwner = new KeyBackup();

    // each custodian 'forwards' their shard to the new secret owner
    for (byte[] custodianPublicKey : custodianPublicKeys.subList(0, threshold - 1)) {
      KeyBackup custodian = custodians.get(custodianPublicKey);
      byte[] forwardMessage = custodian.forwardShard(newSecret.getRootId(), newSecretOwner.getPublicEncryptionKey());
      newSecretOwner.receiveDarkCrystalMessage(forwardMessage, custodianPublicKey, custodian.getPublicSigningKey());
    }

    // the new secret owner
    byte[] recoveredSecret = new byte[0];
    try {
      recoveredSecret = newSecretOwner.getOwnSecret(newSecret.getRootId()).combine();
    } catch (Exception error) {
      assertEquals("Expected error message", error.getMessage(), "Decryption failed");
    }
    assertEquals("Secret not recovered", 0, recoveredSecret.length);
  }

//  @Test
//  @DisplayName("Test forwarding of shards without requesting - handle single mismatched author public key")
//  public void testMismatchedPk() throws Exception {
//    byte[] secret = "its nice to be important but its more important to be nice".getBytes();
//    final KeyBackup secretOwner = new KeyBackup();
//    assertEquals(secretOwner.getPublicEncryptionKey().length, 32, "Public key of correct length available");
//    final int numberCustodians = 4;
//    final int threshold = 3;
//
//    HashMap<byte[], KeyBackup> goodCustodians = createCustodians(numberCustodians - 1);
//    KeyBackup badCustodian = new KeyBackup();
//    byte[] badCustodianPk = badCustodian.getPublicEncryptionKey();
//
//    // the secret owner creates a secret
//    List<byte[]> custodianPublicKeys = new ArrayList<>(goodCustodians.keySet());
//    custodianPublicKeys.add(badCustodianPk);
//    OwnSecret newSecret = secretOwner.createSecret(secret, "a great secret", threshold, custodianPublicKeys);
//    assertEquals(secretOwner.getOwnSecrets().size(), 1, "secret owner has one secret");
//
//    // the malicious custodian meddles with their shard
//    byte[] wrongSecretBytes = "this is wrong".getBytes();
//    final KeyBackup wrongSecretOwner = new KeyBackup();
//    OwnSecret wrongSecret = wrongSecretOwner.createSecret(wrongSecretBytes, "a great secret", threshold, custodianPublicKeys);
//    byte[] evilShardMessage = new Publish.BuildMessage().buildShard(
//            newSecret.getRootId(), // the correct root id
//            badCustodianPk,
//            wrongSecret.getShardMessage(badCustodianPk)); // the wrong shard
//    // the wrong shard has a valid signature - but signed by the wrong public key
//// todo:
//    // we 'send' the shard messages to each custodian
//    for (byte[] custodianPublicKey : custodianPublicKeys) {
//      KeyBackup custodian = custodians.get(custodianPublicKey);
//      custodian.receiveDarkCrystalMessage(
//              newSecret.getShardMessage(custodianPublicKey),
//              secretOwner.getPublicEncryptionKey(),
//              secretOwner.getPublicSigningKey()
//      );
//    }
//
//    // at this point the secret owner looses their device, and needs to recover
//    // lets call the new secret owner newSecretOwner
//    final KeyBackup newSecretOwner = new KeyBackup();
//
//    // each custodian 'forwards' their shard to the new secret owner
//    for (byte[] custodianPublicKey : custodianPublicKeys) {
//      KeyBackup custodian = custodians.get(custodianPublicKey);
//      byte[] forwardMessage = custodian.forwardShard(newSecret.getRootId(), newSecretOwner.getPublicEncryptionKey());
//      newSecretOwner.receiveDarkCrystalMessage(forwardMessage, custodianPublicKey, custodian.getPublicSigningKey());
//    }
//
//    // the new secret owner
//    byte[] recoveredSecret = newSecretOwner.getOwnSecret(newSecret.getRootId()).combine();
//    assertTrue(Arrays.equals(secret, recoveredSecret), "Secret successfully recovered");
//  }
}
