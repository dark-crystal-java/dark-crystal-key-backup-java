package org.magmacollective.darkcrystal.keybackup;

import org.magmacollective.darkcrystal.keybackup.messageschemas.RequestV1.Request;

/**
 * Requests **FROM** ourselves **TO** others
 */
public class OwnRequest {
  private OwnSecret ownSecret;
  private boolean open;
  private byte[] branch;
  private Request requestMessage;

  public OwnRequest(OwnSecret ownSecret, Request requestMessage) {
    this.ownSecret = ownSecret;
    this.requestMessage = requestMessage;
    // TODO how to get the branch (hash of request message?)
    this.open = true;
  }

  /**
   * Close this request (meaning it has been replied to)
   */
  public void closeRequest() {
    open = false;
  }

  /**
   * Check if this request is currently 'open' (still not been replied to)
   * @return true if the request is open.
   */
  public boolean isOpen() {
    return open;
  }

  /**
   * Getter method for 'branch' property (a reference to this request)
   * @return the branch property
   */
  public byte[] getBranch() {
    return branch;
  }
}
