package org.magmacollective.darkcrystal.keybackup;

import org.magmacollective.darkcrystal.keybackup.messageschemas.Publish.BuildMessage;
import org.magmacollective.darkcrystal.keybackup.messageschemas.ShardV1.Shard;
import org.magmacollective.darkcrystal.keybackup.crypto.KeyBackupCrypto;

/**
 * Receives a shard **FROM** others **TO** us
 */
public class ReceivedShard {
  private Shard shardMessage;
  private byte[] authorEncryptionPublicKey;
  private byte[] authorSigningPublicKey;

  public ReceivedShard(Shard shardMessage, byte[] authorEncryptionPublicKey, byte[] authorSigningPublicKey) {
    this.shardMessage = shardMessage;
    this.authorEncryptionPublicKey = authorEncryptionPublicKey;
    this.authorSigningPublicKey = authorSigningPublicKey;
  }

  public Shard getShardMessage() {
    return shardMessage;
  }

  public byte[] getAuthorEncryptionPublicKey() {
    return authorEncryptionPublicKey;
  }

  public byte[] getShardData() {
    return shardMessage.getShard().toByteArray();
  }

  public byte[] getRootId() {
    return shardMessage.getRoot().toByteArray();
  }

  /**
   * Create a forward message for this shard, with no given branch id
   * @param recipient the public encryption key of the recipient
   * @return the forward message
   */
  public byte[] createForward(byte[] recipient) {
    return new BuildMessage().buildForward(recipient, getRootId(), getShardData(), authorSigningPublicKey);
  }
}