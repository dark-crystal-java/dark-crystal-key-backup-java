package org.magmacollective.darkcrystal.keybackup;

import org.magmacollective.darkcrystal.keybackup.messageschemas.RequestV1.Request;
import org.magmacollective.darkcrystal.keybackup.crypto.KeyBackupCrypto;

/**
 * Requests **FROM** others **TO** us
 */
public class ReceivedRequest {
  private boolean open;
  private byte[] branch;
  private Request requestMessage;
  private byte[] authorID;

  public ReceivedRequest(Request requestMessage, byte[] authorID) {
    this.requestMessage = requestMessage;
    this.branch = KeyBackupCrypto.blake2b(requestMessage.toByteArray());
    this.open = true;
  }

  /**
   * Close this request (meaning it has been replied to)
   */
  public void closeRequest() {
    open = false;
  }

  /**
   * Check if this request is currently 'open' (still not been replied to)
   * @return true if the request is open.
   */
  public boolean isOpen() {
    return open;
  }

  /**
   * Getter method for 'branch' property (a reference to this request)
   * @return the branch property
   */
  public byte[] getBranch() {
    return branch;
  }

  public byte[] getAuthorID() {
    return authorID;
  }

  public byte[] getRootID() {
    return requestMessage.getRoot().toByteArray();
  }
}
