package org.magmacollective.darkcrystal.keybackup;

import org.magmacollective.darkcrystal.keybackup.crypto.KeyBackupCrypto;
import org.magmacollective.darkcrystal.keybackup.messageschemas.Publish.BuildMessage;
import org.magmacollective.darkcrystal.keybackup.messageschemas.RequestV1.Request;
import org.magmacollective.darkcrystal.secretsharingwrapper.SecretSharingWrapper;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.magmacollective.darkcrystal.keybackup.crypto.KeyBackupCrypto.toHexString;

/**
 * Secrets which are 'ours' (as opposed to shards held for someone else)
 */
public class OwnSecret {
  private KeyBackup keyBackup;
  private Integer thresholdShards;
  private final byte[] rootId;
  private byte[] rootMessage;
  private Set<byte[]> shards;
  private List<byte[]> custodians;
  private Map<byte[], byte[]> shardMessages;
  private List<OwnRequest> ownRequests;
  private final Map<String, PublicKey> custodiansVerified = new HashMap<>();
  private final String ownPublicEncryptionKey;

  /**
   * @param secret          = this is the actual secret to be sharded
   * @param name            = string describing secret. useful in the case that there are two possible things to shard, in context of briar e.g. are you sharding the contact list or your private key. this name is stored only locally, only the hash of the whole message (which includes name, but not only) is appended to the shards
   * @param thresholdShards = user-chosen number of shards
   * @param custodians      = the people the user wants to send shards to
   * @throws Exception publish class = publish turns the thing into protobuff encoding, which is the encoding we are using to encode the message (could also use json, have implemented that too)
   */
  public OwnSecret(KeyBackup keyBackup, byte[] secret, String name, Integer thresholdShards, List<byte[]> custodians) throws Exception {
    this.keyBackup = keyBackup;
    int numberShards = custodians.size();
    this.thresholdShards = thresholdShards;
    this.custodians = custodians;
    final byte[] tag = KeyBackupCrypto.generateSymmetricKey();
    rootMessage = new BuildMessage().buildRoot(thresholdShards, numberShards, name, tag);
    rootId = KeyBackupCrypto.blake2b(rootMessage);
    shards = new HashSet<>(SecretSharingWrapper.shareAndSign(secret, numberShards, thresholdShards, keyBackup.signingKeyPair.getPrivate()));
    shardMessages = new HashMap<>();
    Iterator<byte[]> shardIterator = this.shards.iterator();
    for (int i = 0; i < numberShards; i++) {
      this.shardMessages.put(this.custodians.get(i), new BuildMessage().buildShard(this.rootId, this.custodians.get(i), shardIterator.next()));
    }
    ownPublicEncryptionKey = toHexString(keyBackup.getPublicEncryptionKey());
    custodiansVerified.put(ownPublicEncryptionKey, keyBackup.getSigningKeyPair().getPublic());
  }

  /**
   * Minimal version of the constructor, for the situation where we
   * are given shards of a secret for which we have no representation
   *
   * @param keyBackup
   * @param rootId
   */
  public OwnSecret(KeyBackup keyBackup, byte[] rootId) {
    this.keyBackup = keyBackup;
    this.rootId = rootId;
    this.shards = new HashSet<>();
    ownPublicEncryptionKey = toHexString(keyBackup.getPublicEncryptionKey());
  }

  /**
   * Get a shard message for a particular custodian
   *
   * @param custodianPublicKey
   * @return a shard message
   */
  public byte[] getShardMessage(byte[] custodianPublicKey) {
    return this.shardMessages.get(custodianPublicKey);
  }

  public boolean canCombine() {
    if ((thresholdShards == null) || (shards.size() >= this.thresholdShards)) {
      try {
        this.combine();
      } catch (Exception err) {
        return false;
      }
      return true;
    } else {
      return false;
    }
  }

  /**
   * Add a shard, and check if we can combine
   *
   * @param shard the shard data
   * @return true if we are able to combine, false otherwise
   */
  public boolean addShard(byte[] shard) {
    shards.add(shard);
    return canCombine();
  }


  public boolean addShardWithAuthorSigningPublicKey(PublicKey publicKey, byte[] authorEncryptionPublicKey, byte[] shard) throws Exception {
    boolean verified = keyBackup.edDSA.verifyMessage(shard, publicKey);
    if (!verified) throw new Exception("Cannot verify given shard");
    custodiansVerified.put(toHexString(authorEncryptionPublicKey), publicKey);
    return addShard(shard);
  }

  /**
   * Create requests for all recipients associated with this secret
   * throws Exception if one or more request messages could not be built
   */
  public void createRequests() throws Exception {
    for (byte[] custodian : custodians) {
      createRequest(custodian);
    }
  }

  /**
   * Create a request for a specific recipient
   *
   * @param recipient
   * @return the encoded request message
   * @throws Exception if the request message could not be built
   */
  public byte[] createRequest(byte[] recipient) throws Exception {
    // TODO should we check that recipient is in this.custodians?
    // TODO Optionally create an ephemeral public key (could depend on a setting set in the constructor?)
    byte[] request = new BuildMessage().buildRequest(recipient, this.rootId);

    // add this request to our list of requests:
    ownRequests.add(new OwnRequest(this, Request.parseFrom(request)));

    return request;
  }

  public byte[] getRootMessage() {
    return this.rootMessage;
  }

  public byte[] getRootId() {
    return this.rootId;
  }

  private PublicKey getAuthorSigningPublicKey() throws Exception {
    // the happy case, where we created the secret ourselves:
    if (custodiansVerified.containsKey(ownPublicEncryptionKey)) {
      return custodiansVerified.get(ownPublicEncryptionKey);
    }

    // now check if everybody gave us the same key:
    final Map<String, PublicKey> publicKeyMap = new HashMap<>();
    for (PublicKey publicKey : custodiansVerified.values()) {
      publicKeyMap.put(toHexString(keyBackup.publicKeyToBytes(publicKey)), publicKey);
    }
    // if they were all the same, great.
    if (publicKeyMap.size() == 1) return publicKeyMap.entrySet().iterator().next().getValue();

    // now we know we have mismatched public keys, lets find which one most custodians gave us
    List<String> publicKeyStrings = new ArrayList<>();
    for (PublicKey publicKey : custodiansVerified.values()) {
      publicKeyStrings.add(toHexString(keyBackup.publicKeyToBytes(publicKey)));
    }
    int highest = 0;
    String winner = "";
    for (String publicKeyString : publicKeyStrings) {
      int frequency = Collections.frequency(publicKeyStrings, publicKeyString);
      if (frequency > highest) {
        highest = frequency;
        winner = publicKeyString;
      }
    }
    // we now know which public key is worth trying - but we need to filter the
    // shards by which ones are signed with this public key
    if (highest > 1) {
      PublicKey winnerPublicKey = publicKeyMap.get(winner);
      shards = shards.stream().filter(shard -> {
        try {
          return keyBackup.edDSA.verifyMessage(shard, winnerPublicKey);
        } catch (Exception e) {
          return false;
        }
      }).collect(Collectors.toSet());
      if (canCombine()) return winnerPublicKey;
    }
    throw new Exception("Mismatched public keys");
  }

  /**
   * Attempt to retrieve the secret
   *
   * @return the secret, if successful
   * @throws Exception if quorum was not met, or one or more shards are invalid
   */
  public byte[] combine() throws Exception {
    PublicKey authorSigningPublicKey = getAuthorSigningPublicKey();

    // Convert set to list
    List<byte[]> shardsList = new ArrayList<>();
    shardsList.addAll(this.shards);

    return SecretSharingWrapper.verifyAndCombine(shardsList, authorSigningPublicKey);
  }

  /**
   * Mark the request with the given branch reference as closed (already replied to)
   *
   * @param branch a reference to a request message
   */
  public void closeRequest(byte[] branch) {
    for (OwnRequest ownRequest : ownRequests) {
      if (ownRequest.getBranch().equals(branch)) {
        ownRequest.closeRequest();
      }
    }
  }
}
