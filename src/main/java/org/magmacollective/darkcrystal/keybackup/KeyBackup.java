package org.magmacollective.darkcrystal.keybackup;

import org.magmacollective.darkcrystal.keybackup.crypto.EdDSA;
import org.magmacollective.darkcrystal.keybackup.crypto.KeyBackupCrypto;
import org.magmacollective.darkcrystal.keybackup.messageschemas.DarkCrystalMessage;
import org.magmacollective.darkcrystal.keybackup.messageschemas.ForwardV1.Forward;
import org.magmacollective.darkcrystal.keybackup.messageschemas.Publish;
import org.magmacollective.darkcrystal.keybackup.messageschemas.ReplyV1.Reply;
import org.magmacollective.darkcrystal.keybackup.messageschemas.RequestV1.Request;
import org.magmacollective.darkcrystal.keybackup.messageschemas.ShardV1.Shard;
import org.whispersystems.curve25519.Curve25519KeyPair;

import java.security.KeyPair;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.magmacollective.darkcrystal.keybackup.crypto.KeyBackupCrypto.toHexString;

/**
 * Dark Crystal Distributed Key Backup
 *
 * @author Magma Collective
 */
public class KeyBackup {
  private Curve25519KeyPair encryptionKeyPair;
  KeyPair signingKeyPair;
  Publish publish;
  private List<OwnRequest> ownRequests;
  private Map<String, OwnSecret> ownSecrets = new HashMap<>();
  private List<ReceivedRequest> receivedRequests;
  private Map<String, ReceivedShard> receivedShards = new HashMap<>();
  public EdDSA edDSA = new EdDSA();
  public final int MAX_MESSAGE_SIZE = 1024 * 1024;

  /**
   * Constructor to be used when no keys exist and so keys need to be generated
   *
   * @throws Exception If there is a problem initialising EdDSA
   */
  public KeyBackup() throws Exception {
    this.signingKeyPair = edDSA.generateKeyPair();
    this.publish = new Publish();
    this.encryptionKeyPair = KeyBackupCrypto.generateCurve25519Keypair();
  }

  /**
   * Constructor with given keys
   *
   * @param signingKeyPair
   * @param encryptionKeyPair
   * @throws Exception If there is a problem initialising EdDSA
   */
  public KeyBackup(KeyPair signingKeyPair, Curve25519KeyPair encryptionKeyPair) throws Exception {
    this.signingKeyPair = signingKeyPair;
    this.publish = new Publish();
    this.encryptionKeyPair = encryptionKeyPair;
  }

  public KeyPair getSigningKeyPair() {
    return this.signingKeyPair;
  }

  public byte[] getPublicSigningKey() {
    return publicKeyToBytes(signingKeyPair.getPublic());
  }

  public byte[] publicKeyToBytes(PublicKey publicKey) {
    return edDSA.PublicKeyToBytes(publicKey);
  }

  public byte[] getPublicEncryptionKey() {
    return this.encryptionKeyPair.getPublicKey();
  }

  public byte[] getPrivateEncryptionKey() {
    return this.encryptionKeyPair.getPrivateKey();
  }

  public OwnSecret getOwnSecret(byte[] rootId) {
    return ownSecrets.get(toHexString(rootId));
  }

  public List<OwnSecret> getOwnSecrets() {
    return new ArrayList<>(this.ownSecrets.values());
  }

  public OwnSecret createSecret(byte[] secret, String name, Integer thresholdShards, List<byte[]> custodians) throws Exception {
    OwnSecret newSecret = new OwnSecret(this, secret, name, thresholdShards, custodians);
    ownSecrets.put(toHexString(newSecret.getRootId()), newSecret);
    return newSecret;
  }

  /**
   * Takes a message, a processes it depending on what type of message it is
   *
   * @param message                        any dark crystal message
   * @param authorEncryptionPublicKey      the encryption public key of the sender
   * @param authorSigningPublicKeyBytes    the signing public key of the sender
   * @throws Exception if the message could not be processed
   */
  public void receiveDarkCrystalMessage(byte[] message, byte[] authorEncryptionPublicKey, byte[] authorSigningPublicKeyBytes) throws Exception {
    if (message.length > MAX_MESSAGE_SIZE)
      throw new Exception(String.format("Messages cannot be more than %d bytes", MAX_MESSAGE_SIZE));

    PublicKey authorSigningPublicKey = edDSA.PublicKeyFromBytes(authorSigningPublicKeyBytes);

    DarkCrystalMessage.DarkCrystal darkCrystalMessage = DarkCrystalMessage.DarkCrystal.parseFrom(message);
    switch (darkCrystalMessage.getMsgCase().name()) {
      case "SHARD":
        receiveShard(darkCrystalMessage.getShard(), authorEncryptionPublicKey, authorSigningPublicKeyBytes);
        break;
      case "REQUEST":
        receiveRequest(darkCrystalMessage.getRequest(), authorEncryptionPublicKey);
        break;
      case "REPLY":
        receiveReply(darkCrystalMessage.getReply(), authorEncryptionPublicKey, authorSigningPublicKey);
        break;
      case "FORWARD":
        receiveForward(darkCrystalMessage.getForward(), authorEncryptionPublicKey, authorSigningPublicKey);
        break;
      default:
        throw new Exception("Unknown message type");
    }
  }

  /**
   * Retrieve a shard we hold for someone else by its rootID
   *
   * @param rootId
   * @return the shard
   * @throws Exception if we do not have a shard with the given root ID
   */
  private ReceivedShard getShardByRootID(byte[] rootId) throws Exception {
    ReceivedShard shard = receivedShards.get(toHexString(rootId));
    if (shard != null) return shard;
    throw new Exception("Shard not found");
  }

  /**
   * Retrieve a request to return a shard with a given branch ID
   *
   * @param branch
   * @return the shard
   * @throws Exception if there is no request with the given branch ID
   */
  private ReceivedRequest getRequestByBranch(byte[] branch) throws Exception {
    for (ReceivedRequest receivedRequest : receivedRequests) {
      if (receivedRequest.getBranch().equals(branch)) {
        return receivedRequest;
      }
    }
    throw new Exception("Request not found");
  }

  /**
   * Respond to a request (TODO)
   *
   * @param branch
   * @throws Exception
   */
  public void respond(byte[] branch) throws Exception {
    ReceivedRequest receivedRequest = getRequestByBranch(branch);
    ReceivedShard receivedShard = getShardByRootID(receivedRequest.getRootID());
  }
  // if yes, check if authorID matches
  // if yes, send a reply message
  // if no, send a forward message
  // if no, do something to indicate that you don't have the requested shard

  /**
   * Create a 'forward' message to forward a shard to a different identity than
   * the original secret owner
   *
   * @param rootId                       a reference to the secret
   * @param recipientEncryptionPublicKey who this message is intended for
   * @return a 'forward' message
   * @throws Exception
   */
  public byte[] forwardShard(byte[] rootId, byte[] recipientEncryptionPublicKey) throws Exception {
    ReceivedShard shard = receivedShards.get(toHexString(rootId));
    if (shard == null) throw new Exception("No secret with the given rootId");
    return shard.createForward(recipientEncryptionPublicKey);
  }

  /**
   * Process a shard message which has been sent to us.
   *
   * @param shardMessage
   * @param authorEncryptionPublicKey the encryption public key of the sender
   * @param authorSigningPublicKey    the signing public key of the sender
   * @throws Exception if the given shard message is not well-formed
   */
  private void receiveShard(Shard shardMessage, byte[] authorEncryptionPublicKey, byte[] authorSigningPublicKey) throws Exception {
    if (!shardMessage.isInitialized()) throw new Exception("Shard badly formed");
    ReceivedShard receivedShard = new ReceivedShard(shardMessage, authorEncryptionPublicKey, authorSigningPublicKey);
    receivedShards.put(toHexString(receivedShard.getRootId()), receivedShard);
  }

  /**
   * Process a request message which has been sent to us
   *
   * @param request
   * @param authorID
   * @throws Exception if the given request message is not well-formed
   */
  private void receiveRequest(Request request, byte[] authorID) throws Exception {
    if (!request.isInitialized()) throw new Exception("Request badly formed");
    ReceivedRequest receivedRequest = new ReceivedRequest(request, authorID);
    receivedRequests.add(receivedRequest);
  }

  /**
   * Process a reply message (returned shard) which has been sent to us.
   * We mark the relevant request as 'closed' (responded to), and add the
   * shard data to our set of shards for the relevant secret.
   *
   * @param reply the given reply message
   * @return true if we have met the threshold (are able to combine and get the secret)
   * @throws Exception
   */
  private boolean receiveReply(Reply reply, byte[] authorEncryptionPublicKey, PublicKey authorSigningPublicKey) throws Exception {
    if (!reply.isInitialized()) throw new Exception("Reply badly formed");

    // Check which secret this reply is for
    byte[] rootId = reply.getRoot().toByteArray();
    byte[] shard = reply.getShard().toByteArray();
    OwnSecret ownSecret = getOrCreateSecretByRootId(rootId);

    // Check branch reference to 'close' request (getBranch)
    byte[] branch = reply.getBranch().toByteArray();
    ownSecret.closeRequest(branch);

    return ownSecret.addShardWithAuthorSigningPublicKey(authorSigningPublicKey, authorEncryptionPublicKey, shard);
  }

  /**
   * Process a 'forward' message.  This is a shard for a secret for which
   * we are not the original author. We treat it in the same way as we
   * treat 'reply' messages.
   *
   * @param forward                   the forward message
   * @param authorEncryptionPublicKey to identify which custodian this was from
   * @return true if we have met the threshold (are able to combine and get the secret)
   * @throws Exception if the given message was not formed correctly
   */
  private boolean receiveForward(Forward forward, byte[] authorEncryptionPublicKey, PublicKey authorSigningPublicKey) throws Exception {
    if (!forward.isInitialized()) throw new Exception("Forward message badly formed");

    // Check which secret this forward is for
    byte[] rootId = forward.getRoot().toByteArray();
    byte[] shard = forward.getShard().toByteArray();
    OwnSecret ownSecret = getOrCreateSecretByRootId(rootId);

    if (forward.hasBranch()) {
      // Check branch reference to 'close' request (getBranch)
      byte[] branch = forward.getBranch().toByteArray();
      ownSecret.closeRequest(branch);
    }

    if (forward.hasSignaturePublicKey()) {
      byte[] publicKeyBytes = forward.getSignaturePublicKey().toByteArray();
      PublicKey signaturePublicKey = edDSA.PublicKeyFromBytes(publicKeyBytes);
      return ownSecret.addShardWithAuthorSigningPublicKey(signaturePublicKey, authorEncryptionPublicKey, shard);
    }

    return ownSecret.addShard(shard);
  }

  /**
   * Given a rootId of a secret which we authored, retrieve the secret
   * object.
   * If there is no secret with the given rootID, we create one.  This is
   * used for the situation where we are 'forwarded' a shard which we do not
   * own.
   *
   * @param rootId
   * @return the secret object
   */
  private OwnSecret getOrCreateSecretByRootId(byte[] rootId) {
    OwnSecret secret = ownSecrets.get(toHexString(rootId));
    if (secret != null) return secret;
    OwnSecret newSecret = new OwnSecret(this, rootId);
    ownSecrets.put(toHexString(rootId), newSecret);
    return newSecret;
  }


}
